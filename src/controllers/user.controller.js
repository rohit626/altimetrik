const express = require('express');
const router = express.Router();
const userService = require('../service/user.service');

router.post('/save', async (req, res)=>{
    try {
        user = await userService.saveUser(req.body);
        res.send(user);
    } catch (error) {
        res.status(400).send(error);
    }
})

router.post('/login', async (req, res)=>{
    try {
        user = await userService.getUser(req.body.username, req.body.password);
        res.send(user);
    } catch (error) {
        res.status(400).send(error);
    }
})

module.exports = router;