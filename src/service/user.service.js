const mongoose = require('mongoose');
const User = require('../models/user');
const passwordHash = require('password-hash');

saveUser = (user) => new Promise ((resolve, reject) =>{
    try {
        const usr = new User(user);
        usr.password = passwordHash.generate(usr.password);
        return usr.save(usr);
    } catch(error) {
        return error;
    }
})

getUser = (username, password) =>  new Promise((resolve, reject)=>{
    User.where({username:username}).findOne(function (err, usr) {
        if (err) reject("Username not found!");
        if (usr) {
            if (passwordHash.verify(password, usr.password)) {
                console.log("Matched user is ", usr);
                delete usr.password;
                resolve(usr);
            } else {
                console.log("Not Matched");
                reject("Password is invalid!");
            }
        } else {
            reject("Username not found!");
        }
    });
})

updateUser = async (id, user) => {
    try {
        var query = { '_id': id };
        return User.findOneAndUpdate(query, user);
    } catch(error) {
        return error;
    }
};

module.exports = {saveUser, getUser}