const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://altimetrik:altimetrik@cluster0.fygq6.mongodb.net/altimetrik?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
})

module.exports = mongoose;
