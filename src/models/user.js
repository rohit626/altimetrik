const mongoose = require("../db/mongoose")
const User = mongoose.model('User', {
    username: {
        type: String, 
        required: true,
        trim: true, 
        lowercase: true, 
        validator(value) {
            if(value === null) {console.log('Please enter username')}
        }
    },
    password: {type: String},
    email: {type:String},
    first_name: {type: String},
    last_name: {type: String},
    gender: {type:Number},
    country: {type:Number}
})

module.exports = User;
